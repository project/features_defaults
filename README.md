INTRODUCTION
------------
The Features Defaults module allows users to select which Features package will
will display by default in the vertical tab on the Manage Features page. It also
allows users to set the default package for new features on the Create Feature
form.

REQUIREMENTS
------------
This module requires the following module:
 * Features (https://www.drupal.org/project/features)

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/documentation/install/modules-themes/modules-7 for
further information.

CONFIGURATION
-------------
This module adds fields to the normal Features Settings at Administration →
Structure → Features → Settings (`admin/structure/features/settings`).

In the Features Default Tab field select the Features package whose vertical tab
should have focus by default on the Manage Features page at Administration →
Structure → Features (`admin/structure/features`).

In the Features Default Package field set the name of the default package for
new features when they are created at Administration → Structure → Features →
Create Feature (`admin/structure/features/create`).

MAINTAINERS
-----------
* Tommy Keswick (TommyK) - https://www.drupal.org/user/382217
